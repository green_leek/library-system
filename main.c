#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stddef.h>
#include <stdbool.h>
#include <limits.h>
#include "header.h"

static struct Books db[USHRT_MAX];
static u_short book_in_db;

static void error(enum Error_l reason) {
    const char * reason_l[] = {
        "Invalid input!",
        "Error while accessing the file",
        "User cancelled.",
        "Invalid character.",
        "Empty list.",
        "Manual Not found.",
        "Id number incorrect."
    };

    fprintf(stderr, "%s\n", reason_l[reason]);
}
static int menu(enum Function_l Selected) {
    typedef const char * name_t;

    struct item_t {
        name_t name;
        int (* const function)();
    };
    struct menu{
        struct item_t * item;
    };

    struct item_t main_l[] = {
        {"Book Management", BookManagement},
        {"Book Search", BookSearch},
        {"Book View", booklist_view},
        {"User Manual", UserManual}
    };
    struct item_t bookmgr_l[] = {
        {"Entry Adder", entry_add},
        {"Entry Deleter", entry_delete},
        {"Entry Editor", entry_editor},
        {"Entry Viewer", booklist_view}
    };
    struct item_t bookserach_l[] = {
        {"Search by id", main}
    };

    typedef const struct menu menu_t;
    menu_t main = {main_l}, bookmgr = {bookmgr_l}, booksearch = {bookserach_l};
    menu_t * const menu_arr[] = {&main, &bookmgr, &booksearch};

    u_short usr_input = 0;
    size_t const list_length = ARRAY_LENGTH(&menu_arr[Selected]->item);
    size_t i;
    do{

        usr_input = 0;

        for(i = 1; i < list_length; i++) {
            printf("%d. %s\n", i, (menu_arr[Selected]->item[i].name));
        }
        i++;
        printf("%d. %s\n", i, Selected == Main ? "Exit" : "Return to Menu");
        prompt_num(&usr_input);

        if(usr_input == i) {
            if(Selected == Main) {
                exit(0);
            }

            Selected = Main;
        }

    } while((usr_input - 1 > list_length) || (usr_input == i));

    return menu_arr[Selected]->item[usr_input - 1].function();
}
int BookSearch(){
    return menu(Book_Search);
}
int load_db_or_die() {

    if(is_file_ok() == true) {
        if(is_file_empty() == false) {
            FILE * fp = booklist_get("r");

            for(book_in_db = 0; book_in_db != line_count(); book_in_db++) {
                fscanf(fp, "%hu|%49[^|]|%49[^|]|%99[^\n]\n", &db[book_in_db].book_id, db[book_in_db].title, db[book_in_db].author, db[book_in_db].subject);
            }

            fclose(fp);
        }

        return 0;
    };

    puts("Cannot load db.");

    abort();
}

int booklist_view() {
    return (booklist_printing(), main());
}
int booklist_printing() {

    if(!is_file_empty()) {
        u_int i;

        for(i = 0; i != book_in_db; i++) {
            print_book(db[i]);
        }

        puts("");
    }

    return 0;
}
int print_book(const struct Books book) {
    return printf("Book id:%u\nBook Title:%s\nBook author:%s\nBook subject:%s\n\n", book.book_id, book.title, book.author, book.subject);
}
bool booklist_reset() {

    puts("Deleting ...");

    if(remove("booklist.txt")) {
        return (error(File), false);
    };

    puts("Book list deleted successfully.\nRebuilding ...\n");

    return is_file_ok();
}

int BookManagement() {
    return menu(Book_Management);
}

inline bool is_file_ok() {
    FILE * fp = booklist_get("r");

    return !fclose(fp);
}

flag prompt_book_item(struct Books * book, const u_int item, const flag symbol, const char * symbol_action) {
    char * const book_item[] = {book->title, book->author, book->subject};
    char * const book_item_str[] = {"title", "author", "subject"};
    char msg[50];

    sprintf(msg, "Please input book %s, input '%c' to %s", book_item_str[item], symbol, symbol_action);

    prompt_string(msg, book_item[item], sizeof(book_item[item]), true);

    return (char)strncmp(book_item[item], &symbol, 1);
}
int entry_add() {

    if(is_file_ok()) {

        struct Books new_book;

        do {
            u_int i = 0;

            for(; i != 3; i++) {
                if(prompt_book_item(&new_book, i, '~', "cancell") == 0) {
                    error(User);
                    return BookManagement();
                }
            }

            new_book.book_id = (u_short)line_count() + 1;

            print_book(new_book);

        } while(!is_user_confirm("Is those correct? (Enter 'Y' to confirm, any key to re-enter.)"));

        ++book_in_db;

        //fprintf return the total number of characters written if successed
        printf("%s\n", entry_write(new_book) > 0 ? "Finish!" : "Failed ...");
    }

    return BookManagement();
}

bool is_user_confirm(const char * const msg) {
    char ok[2];

    prompt_string(msg, ok, sizeof(ok), false);
    //NAND (Not AND) gate
    return !((strncmp(ok, "y", 1) & (strncmp(ok, "Y", 1))));
}
int entry_delete() {

    if(!is_file_empty()) {
        char id[6];

        booklist_printing();

        prompt_string("Please input id to delete.\nInput 'all' to reset, Input '~' to cancel."
                      , id, sizeof(id), true);

        if(is_user_break(id) == true) {
            error(User);
            return BookManagement();
        }

        if(strncmp(id, "all", 3) == 0) {

            if(is_user_confirm("Are you sure? This operation will delete *ALL* entry.") == true) {
                booklist_reset();
            } else {
                error(User);
            }

            return BookManagement();
        }

        if(atoi(id) < (int)(USHRT_MAX & book_in_db)) {
            printf("%s\n", entry_erase((u_short)atoi(id)) == 0 ? "Done !" : "Failed...");
        } else {
            error(Id);
        }
    }

    return BookManagement();
}
int entry_erase(const u_short book_id) {

    struct Books del_book;

    del_book.book_id = book_id;
    strcpy(del_book.author, "*deleted*");
    strcpy(del_book.subject, "*deleted*");
    strcpy(del_book.title, "*deleted*");

    return entry_rewrite(del_book);
}

inline static bool is_user_break(const char * const string) {
    return strncmp(string, "~", 1) == 0 ? (error(User), true) : false;
}
int entry_editor() {

    if(!is_file_empty()) {
        struct Books book;

        do {
            booklist_printing();

            puts("Please input book id to edit.");

            prompt_num(&book.book_id);

            u_int i = 0;

            for(; i != 3; i++) {
                const char * book_item[] = {book.title, book.author, book.subject};
                const char * db_item[] = {db[i].title, db[i].author, db[i].subject};

                if(prompt_book_item(&book, i, '-', "keep the oringal") == 0) {
                    book_item[i] = db_item[i];
                }
            }

        } while(!is_user_confirm("Is those correct? (Enter 'Y' to confirm, any key to re-enter.)"));

        printf("%s\n", entry_write(book) > 0 ? "Finish!" : "Failed ...");
    }

    return BookManagement();
}
size_t line_count() {

    size_t line = 0;

    if(is_file_empty() == false) {
        FILE * fp = booklist_get("r");

        int ch = 0;

        for(; (ch = fgetc(fp)) != EOF; line += (ch == '\n') ? 1 : 0);

        fclose(fp);
    }

    return line;
}

bool is_file_empty() {
    if(!is_file_ok() == false) {
        FILE * fp = booklist_get("a");

        fseek(fp, 0, SEEK_END);

        if(ftell(fp) == 0) {
            error(List);
            return true;
        }

        fclose(fp);
    }

    return false;
}
int entry_write(const struct Books book) {
    FILE * fp = booklist_get("a");
    return fprintf(fp, "%u|%s|%s|%s\n", book.book_id, book.title, book.author, book.subject) + fclose(fp);
}
int entry_rewrite(const struct Books book) {

    if(is_file_ok() == true && booklist_reset() == true) {

        u_int i = 1;

        for(; i != book_in_db; i++) {
            entry_write(i == book.book_id ? book : db[i]);
        }

        return 0;
    }

    return 1;
}
FILE * booklist_get(const char * mode) {

    FILE * fp = fopen("booklist.txt", mode);

    if(fp == NULL) {

        puts("Creating Book list...");
        fopen("booklist.txt", "w+");

        if(fp == NULL) {
            error(File);
            abort();
        }

        freopen("booklist.txt", mode, fp);
        puts("Book list Created.");
    }

    return fp;
}
unsigned short prompt_num(unsigned short * const number) {

    prompt_symbol();

    scanf("%hu", number);
    return *number;
}
char * prompt_string(const char * const msg, char * usr_input, const size_t max_length, const bool cheak_validity) {

    do {
        printf("%s", msg);

        prompt_symbol();

        fgets(usr_input, (int)max_length, stdin);

        if(!cheak_validity && usr_input[0] != '\n') {
            break;
        }
    } while(input_validation(usr_input) == false);

    if(usr_input[strlen(usr_input) - 1] == ' ' || usr_input[strlen(usr_input) - 1] == '\n') {
        usr_input[strlen(usr_input) - 1] = '\0';
    }

    return usr_input;
}
bool input_validation(const char * const input) {

    if(strnlen(input, 1) != 0) {
        int i = 0;

        for(; input[i] != '\0'; i++) {

            if(strncmp(&input[i], "|", 1) == 0) {
                printf("%s", input);
                return (error(Character), false);
            }
        }

        return true;
    }

    return false;
}
int UserManual() {
    FILE * fp = fopen("user_manual.txt", "r");

    if(fp != NULL) {
        int ch;

        for(; (ch = fgetc(fp)) != EOF; printf("%c", ch));

        puts("");

        fclose(fp);

    } else {
        error(Manual);
    }

    return main();
}

int main() {
    return load_db_or_die(), menu(Main);
}
