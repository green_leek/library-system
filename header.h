#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#define ARRAY_LENGTH(array) sizeof(array) / sizeof(array[0])
#define VNAME(x) #x

typedef char flag;
typedef unsigned short u_short;
typedef unsigned int u_int;

struct Books {
    char title  [50];
    char author [50];
    char subject[100];
    u_short book_id;
};

int main(void);
//
inline int booklist_view(void);
int        booklist_printing(void);
int        print_book(const struct Books);
bool       booklist_reset(void);
FILE   *   booklist_get(const char * const);
//
int BookManagement(void);
int BookSearch(void);
int entry_add(void);
int entry_rewrite(const struct Books);
int entry_erase(const u_short book_id);
int entry_delete(void);
int entry_editor(void);
int entry_write(const struct Books);
//
unsigned short prompt_num(unsigned short * const);
char     *     prompt_string(const char * const, char *, const size_t, const bool);
inline int     prompt_symbol(void);
flag prompt_book_item (struct Books * book, const u_int, const flag, const char *);

bool input_validation(const char * const);
size_t line_count(void);

bool is_file_empty(void);
inline bool is_file_ok(void);
bool is_user_confirm(const char * const);
inline static bool is_user_break(const char * const);

int load_db_or_die(void);
int UserManual(void);

inline int prompt_symbol() {
    return (fflush(stdin), fseek(stdin, 0, SEEK_END), printf("\n> "));
}

enum Function_l {
    Main,
    Book_Management,
    Book_Search,
    User_Manual
};
enum Error_l {
    Input,
    File,
    User,
    Character,
    List,
    Manual,
    Id
};

#endif // HEADER_H_INCLUDED
